<?php

declare(strict_types=1);

namespace Drupal\dkan_rss\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;

/**
 * Provides a rss link block.
 *
 * @Block(
 *   id = "dkan_rss_rss_link",
 *   admin_label = @Translation("RSS Link"),
 *   category = @Translation("Custom"),
 * )
 */
final class RssLinkBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $url = Url::fromRoute('dkan_rss.dataset_rss');
    $build['content'] = [
      '#theme' => 'dkan_rss_link',
      '#link' => $url->toString(),
      '#text' => $this->t('RSS link')
    ];
    return $build;
  }

}
