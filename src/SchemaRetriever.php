<?php

namespace Drupal\dkan_rss;

class SchemaRetriever extends \Drupal\metastore\SchemaRetriever
{
  /*
   * allow for the dataset.rss.feed.json to be used
   */
  public function getAllIds()
  {
    $allids = parent::getAllIds();
    $allids[] = 'dataset.rss.feed';
    return $allids;
  }
}
