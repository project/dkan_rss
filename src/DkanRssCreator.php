<?php

namespace Drupal\dkan_rss;

use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Extension\ExtensionList;
use Drupal\Core\Url;
use Drupal\facets\Exception\Exception;
use Drupal\metastore\MetastoreService;
use Drupal\metastore\Storage\DataFactory;
use Drupal\node\NodeInterface;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class DkanRssCreator {
  /**
   * @var SchemaRetriever
   */
  protected $schemaRetriever;

  /**
   * @var MetastoreService
   */
  protected $metastoreService;

  /**
   * @var DataFactory
   */
  protected $storage;

  /**
   * @var Serializer
   */
  protected $serializer;

  /**
   * @var EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * @var ExtensionList
   */
  protected $extensionListModule;

  protected $sortProperty;

  /**
   * @param SchemaRetriever $schema_retriever
   * @param MetastoreService $metastore_service
   * @param DataFactory $metastore_storage
   * @param EntityRepositoryInterface $entity_repository
   * @param ExtensionList $extension_list
   */
  public function __construct(SchemaRetriever $schema_retriever, MetastoreService $metastore_service, DataFactory $metastore_storage, EntityRepositoryInterface $entity_repository, ExtensionList $extension_list)
  {
    $this->schemaRetriever = $schema_retriever;
    $this->metastoreService = $metastore_service;
    $this->storage = $metastore_storage;
    $encoders = [new XmlEncoder([
      'xml_root_node_name' => 'rss',
    ])];
    $this->entityRepository = $entity_repository;
    $this->extensionListModule = $extension_list;
    $normalizers = [new ObjectNormalizer()];
    $this->serializer = new Serializer($normalizers, $encoders);
  }

  /**
   * create the RSS feed from datasets and rss schema
   *
   * @return string
   *   serialized xml string from rss schema object
   */
  public function datasetRss() {
    $datasets = $this->retrieveDatasets();
    $rssSchema = json_decode($this->rssSchema());
    $this->addAtomLink($rssSchema->channel);
    $items = [];
    foreach ($datasets as $dataset) {
      $item = new \stdClass();
      $dataset = json_decode($dataset);
      foreach ($rssSchema->channel->item as $item_key => $item_value) {
        if (is_object($item_value)) {
          $value = null;
          $this->nestedProperty($item_value, clone $dataset, $value);
          $item->{$item_key} = $value;
        }
        else {
          if (isset($dataset->{$item_value})) {
            $item->{$item_key} = $dataset->{$item_value};
          }
          else {
            if (method_exists($this, $item_value)) {
              $item->{$item_key} = $this->{$item_value}($dataset);
            }
          }
        }
      }
      $this->removeEmptyItems($item);
      foreach ($rssSchema->channel->item->_formatters as $item_key => $formatter) {
        if (isset($item->{$item_key})) {
          if (is_string($formatter) && method_exists($this, $formatter)) {
            $item->{$item_key} = $this->{$formatter}($item->{$item_key});
          }
        }
      }
      foreach ($rssSchema->channel->item->_attributes as $item_key => $attribute) {
        if (isset($item->{$item_key})) {
          if (is_string($item->{$item_key}) && is_object($attribute)) {
            $wrapper = clone $attribute;
            $wrapper->{'#'} = $item->{$item_key};
            $item->{$item_key} = $wrapper;
          }
        }
      }
      $items[] = $item;
    }
    if (isset($rssSchema->_sort)) {
      $sort = (array) $rssSchema->_sort;
      $this->sortProperty = key($sort);
      uasort($items, [$this, $sort[$this->sortProperty]]);
      unset($rssSchema->_sort);
    }
    if (isset($rssSchema->_limit)) {
      $limit = $rssSchema->_limit;
      if (is_int($limit) && $limit !== 0) {
        $items = array_slice($items, 0, $limit);
      }
      unset($rssSchema->_limit);
    }
    $rssSchema->channel->item = $items;
    $this->removeEmptyItems($rssSchema->channel);
    return $this->serializer->serialize($rssSchema, 'xml');
  }

  /**
   * recursive processing of rss feeds properties, allowing for more
   * sophisticated rss schema defintion
   * @param $property
   * @param $data
   * @param $value
   * @return void
   */
  protected function nestedProperty($property, $data, &$value) {
    if (is_object($property)) {
      foreach ($property as $subkey => $subproperty) {
        if (isset($data->{$subkey})) {
          $this->nestedProperty($subproperty, $data->{$subkey}, $value);
        }
      }
    }
    else {
      if (is_string($value)) {
        $value .= ' ' . $data;
      }
      else {
        $value = $data;
      }
    }
  }

  /**
   * add link to feed as atom:link XML tag as proposed by W3C RSS feed validator
   *
   * @param $channel
   * @return void
   */
  protected function addAtomLink($channel) {
    $atom_link  = new \stdClass();
    $atom_link->{'@href'} = Url::fromRoute('dkan_rss.dataset_rss', [], ['absolute' => TRUE])->toString();
    $atom_link->{'@rel'} = 'self';
    $atom_link->{'@type'} = 'application/rss+xml';
    $channel->{'atom:link'} = $atom_link;
  }

  /**
   * convert date for date according to RFC822/RFC2822 as proposed by W3C RSS
   * feed validator
   *
   * @param $date_string
   * @return mixed|string
   */
  protected function rfc822Date($date_string) {
    try {
      $date = new \DateTime($date_string);
      return $date->format(\DateTime::RFC2822);
    }
    catch (\Exception $exception) {
      return $date_string;
    }
  }

  /**
   * remove empty properties from the RSS/XML object representation
   *
   * @param object $item
   * @return void
   */
  protected function removeEmptyItems(object $item) {
    foreach ($item as $item_key => $item_value) {
      if (empty($item_value)) {
        unset($item->{$item_key});
      }
    }
  }

  /**
   * retrieve all datasets from the metastore storage
   *
   * @return array|string[]
   */
  protected function retrieveDatasets() {
    return $this->storage->getInstance('dataset')->retrieveAll();
  }

  /**
   * retrieve RSS schema, with fallback to local file if the schema.json does
   * not exist
   * @return false|string|null
   */
  protected function rssSchema() {
    try {
      return $this->schemaRetriever->retrieve('dataset.rss.feed');
    }
    catch (\Exception $exception) {
      return file_get_contents($this->extensionListModule->getPath('dkan_rss') . '/schema/dataset.rss.feed.json');
    }
  }

  /**
   * get an link to an rss item by loading item's entity via UUID
   *
   * @param $item
   * @return \Drupal\Core\GeneratedUrl|false|string
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function rss_item_link($item) {
    if (isset($item->identifier)) {
      $dataset_node = $this->entityRepository->loadEntityByUuid('node', $item->identifier);
      if ($dataset_node instanceof NodeInterface) {
        return $dataset_node->toUrl('canonical', ['absolute' => true])->toString();
      }
    }
    return FALSE;
  }

  /**
   * sort items ascending (uasort callback)
   *
   * @param $a
   * @param $b
   *
   * @return int
   */
  protected function baseSortAsc($a, $b) {
    return $a->{$this->sortProperty} <=> $b->{$this->sortProperty};
  }

  /**
   * sort items descending (uasort callback)
   *
   * @param $a
   * @param $b
   *
   * @return int
   */
  protected function baseSortDesc($a, $b) {
    return $a->{$this->sortProperty} <=> $b->{$this->sortProperty};
  }

  /**
   * sort items by date (descending, newest date first)
   *   (uasort callback)
   *
   * @param $a
   * @param $b
   *
   * @return int
   * @throws \Exception
   */
  protected function dateSort($a, $b) {
    try {
      $a_date = new \DateTime($a->{$this->sortProperty});
      $b_date = new \DateTime($b->{$this->sortProperty});
    }
    catch (Exception $exception) {
      return 0;
    }
    return $b_date <=> $a_date;
  }
}
