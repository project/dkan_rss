<?php

namespace Drupal\dkan_rss\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\dkan_rss\DkanRssCreator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Rss Feed Controller
 */
class DkanRssController implements ContainerInjectionInterface {

  /**
   * @var DkanRssCreator
   */
  protected $rssCreator;

  /**
   * @param ContainerInterface $container
   * @return static
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('dkan_rss.rss_creator.service')
    );
  }

  /**
   * @param DkanRssCreator $rss_creator
   */
  public function __construct(DkanRssCreator $rss_creator) {
   $this->rssCreator = $rss_creator;
  }

  /**
   * dataset rss feed response
   *
   * @return Response
   */
  public function datasetRss() {
    $response = new Response();

    $response->headers->set('Content-Type', 'application/rss+xml');
    $response->setContent($this->rssCreator->datasetRss());

    return $response;
  }
}
