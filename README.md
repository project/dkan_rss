# DKAN dataset RSS feed
Provide an RSS feed of datasets from DKANv2

## Features
The module provides an RSS Feed of the datasets from DKANv2 metastore at "/datasets.rss".

## Post-Installation
the module will provide an RSS feed at datasets.rss after installation. The RSS feed can be customized by copying the file dataset.rss.feed.json to the schema location of DKANv2 and editing the file there. The dataset.rss.feed.json is related to DKANs dataset.json. You can link properties from dataset.json to RSS feeds items.

So for example if the property "issued" exists in the dataset.json you can assing this property to RSS items "pubDate". There are also more sophisticated assignments possibly, look for example to the "dc:creator" property in the dataset.rss.feed.json. Additionally there are some advanced modifications possible via the "_attributes" and "_formatters" properties in the dataset.rss.feed.json

The module should produce an valid RSS feed with the default DKAN installation (for example with demo content).

## Additional Requirements
- [DKANv2](https://github.com/GetDKAN/dkan)
- symfony/property-access

